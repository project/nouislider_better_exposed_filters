(function (Drupal, once) {
  "use strict";

  function filterPips(value, type) {
    if (type === 0) {
      return Math.round(value) % 5 === 0 ? 2 : 0;
    }
    return 1;
  }

  Drupal.behaviors.tbdiahVefNouiSlider = {
    attach: function (context) {
      once('nouislider-initialized', '[data-nouislider]', context).forEach(
        (slider) => {
          const dataset = slider.dataset;
          const min = parseInt(dataset.nouisliderMin);
          const max = parseInt(dataset.nouisliderMax);
          const selectedValues = dataset.nouisliderSelectedValues ? JSON.parse(dataset.nouisliderSelectedValues) : [];
          const length = selectedValues.length;
          const hasDefaultValue = length > 1;
          const pipsOpts = {
            mode: dataset.nouisliderPipsMode,
            density: 2,
            stepped: true,
          };

          if (dataset.nouisliderPipsFilter === 'true') {
            pipsOpts['filter'] = filterPips;
          }

          noUiSlider.create(slider, {
            start: [
              hasDefaultValue ? parseInt(selectedValues[0]) : min,
              hasDefaultValue ? parseInt(selectedValues[length - 1]) : max,
            ],
            connect: true,
            step: 1,
            tooltips: true,
            format: {
              to: function (value) {
                return Number(value);
              },
              from: function (value) {
                return Number(value);
              },
            },
            range: {
              'min': min,
              'max': max,
            },
            pips: pipsOpts,
          });

          // Mapping of {<value>:<entity_reference_id>}.
          const mappingValueIds = JSON.parse(dataset.nouisliderValueEntityReferenceIdMapping);
          const wrapper = slider.closest('.nouislider-element-wrapper');
          const select = wrapper.querySelector(`.form-type-select [data-nouislider-target-key="${dataset.nouisliderTargetKey}"]`);
          slider.noUiSlider.on('update', function (values, handle) {
            const currentMin = values[0];
            const currentMax = values[1];
            let selected = [];

            // Set the selected elements.
            Object.keys(mappingValueIds).forEach(value => {
              const referenceId = mappingValueIds[value];
              if (value < currentMin || value > currentMax) {
                return;
              }
              selected.push(referenceId);
            });

            // Assign the selected option elements.
            Array.from(select.options).forEach(function (option) {
              // If the option's value is in the selected array, select it
              // Otherwise, deselect it.
              option.selected = selected.includes(parseInt(option.value));
            });
          });
        },
      );
    },
  };

})(Drupal, once);
