# Drupal - NoUISlider

This is a [noUiSlider](https://github.com/leongersen/nouislider) integration for Drupal. It provides a noUiSlider
element for select list and entity reference fields.


For a full description of the module, visit the
[project page](https://www.drupal.org/project/nouislider_better_exposed_filters).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/nouislider_better_exposed_filters).

## Table of contents

- Dependencies
- Requirements
- Installation
- Configuration
- Maintainers

## Dependencies

* [NoUiSlider library](https://github.com/leongersen/noUiSlider) (>=15.7.x)

## Requirements

This module requires the following modules:

- [Better Exposed Filters](https://www.drupal.org/project/better_exposed_filters)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).
Also it's needed to get the noUiSlider library.

### Composer (recommended)
If you would like to install the noUiSlider library with composer, you probably used the
[drupal composer template](https://github.com/drupal-composer/drupal-project) to setup your project.

After this you can install the library with "composer require oomphinc/composer-installers-extender npm-asset/nouislider"
and the library will be downloaded into the libraries folder.

### Manual
Download it from the [release page](https://github.com/leongersen/noUiSlider/releases) and place it in Drupal's library folder.

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will add a new option (NoUiSlider) to Better Exposed Filter settings
inside the View.

## Maintainers

- Mamadou Diao Diallo - [diaodiallo](https://www.drupal.org/u/diaodiallo)
- Nia Kathoni - [nikathone](https://www.drupal.org/u/nikathone)
- Daniel Cothran - [andileco](https://www.drupal.org/u/andileco)
