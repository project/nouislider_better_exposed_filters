<?php

namespace Drupal\nouislider_better_exposed_filters\Plugin\better_exposed_filters\filter;

use Drupal\better_exposed_filters\Plugin\better_exposed_filters\filter\FilterWidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * NoUiSlider widget implementation.
 *
 * @BetterExposedFiltersFilterWidget(
 *   id = "bef_nouislider",
 *   label = @Translation("NoUiSlider"),
 * )
 */
class nouislider extends FilterWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['advanced']['pips_mode'] = 'range';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $form_state): void {

    $field_id = $this->getExposedFilterFieldId();

    parent::exposedFormAlter($form, $form_state);

    if (!empty($form[$field_id])) {

      if (empty($form[$field_id]['#multiple']) || $form[$field_id]['#type'] !== 'select') {
        return;
      }

      // Adding a prefix to wrap the field and the nouislider container in the
      // same div.
      $form[$field_id]['#prefix'] = '<div class="nouislider-element-wrapper">';
      // Hiding the select fields.
      $form[$field_id]['#attributes']['class'][] = 'd-none';
      $form[$field_id]['#attributes']['data-noUiSlider-target-key'] = $field_id;
      // Getting the options from the field so that they can be used for noUI slider.
      $field_options = array_flip($form[$field_id]['#options']);
      // Getting the position of where to place the nouislider element inside the
      // form element.
      $position = array_search($field_id, array_keys($form)) + 1;
      $nouislider_key = $field_id . '_nouislider';
      // Get default value.
      /** @var \Drupal\views\ViewExecutable $view */
      $view = $form_state->get('view');
      $exposed_inputs = $view->getExposedInput();
      $selected_ref_ids = $exposed_inputs[$field_id] ?? [];
      // Check if the user selected default values.
      if (!$selected_ref_ids && !empty($form[$field_id]['#default_value'])) {
        $selected_ref_ids = $form[$field_id]['#default_value'];
      }

      $selected_values = [];
      if (is_array($selected_ref_ids)) {
        foreach ($selected_ref_ids as $ref_id) {
          if (!isset($form[$field_id]['#options'][$ref_id])) {
            continue;
          }
          $selected_values[] = $form[$field_id]['#options'][$ref_id];
        }
        asort($selected_values);
        asort($selected_values);
      }

      $pips_filter = $this->configuration['advanced']['pips_mode'] == 'range' ? 'true' : 'false';

      // Setting the nouislider element.
      $nouislider_element = [$nouislider_key => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attached' => ['library' => ['nouislider_better_exposed_filters/nouislider-init']],
        '#attributes' => [
          'data-noUiSlider' => TRUE,
          'data-noUiSlider-min' => array_key_first($field_options),
          'data-noUiSlider-max' => array_key_last($field_options),
          'data-noUiSlider-value-entity-reference-id-mapping' => json_encode($field_options),
          'data-noUiSlider-target-key' => $field_id,
          'data-noUiSlider-pips-mode' => $this->configuration['advanced']['pips_mode'],
          'data-noUiSlider-pips-filter' => $pips_filter,
          'data-noUiSlider-selected-values' => $selected_values ? json_encode($selected_values) : NULL,
        ],
        '#suffix' => '</div>',
      ]];
      // Insert the nouislider element after the targeted element.
      $form = array_merge(array_splice($form, 0, $position), $nouislider_element, $form);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildConfigurationForm($form, $form_state);

    $form['advanced']['pips_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Pips mode'),
      '#options' => ['range' => 'Range', 'steps' => 'Steps'],
      '#required' => TRUE,
      '#description' => $this->t('The NoUiSliger pips mode.'),
      '#default_value' => $this->configuration['advanced']['pips_mode'],
    ];

    return $form;
  }

}
